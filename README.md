# インターネットガールズバー

See https://internetgirlsbar.tk.

Fork of https://github.com/mdn/web-speech-api/tree/master/speech-color-changer.

# Depends on:

* https://gitlab.com/todeslang/todeslang

# Install

```
cd .../src
sudo make install
```

Run in screen command:

```
todesweb internetgirlsbar
```

```
cd .../caddy
sudo caddy run
```
