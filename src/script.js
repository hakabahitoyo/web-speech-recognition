var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition

const recognition = new SpeechRecognition ()
recognition.continuous = true
recognition.lang = 'ja-JP'
recognition.interimResults = false
recognition.maxAlternatives = 1

const synth = window.speechSynthesis


window.addEventListener ('load', function () {
	document.getElementById ('button-start').focus ()
}, false)


window.addEventListener ('load', function() {
document.getElementById ('button-start').addEventListener ('click', function () {
	if (! document.getElementById ('checkbox-smartphone').checked) {
		document.getElementById ('button-start').hidden = true
	}
	startRecognition ()
}, false) /* document.getElementById ('button-start').addEventListener ('click', function () { */
}, false) /* window.addEventListener ('load', function() { */


function startRecognition () {
	recognition.start();
	console.log('Listening.');

	if (! document.getElementById ('checkbox-smartphone').checked) {
		let p = document.createElement ('p')
		let b = document.createElement ('b')
		p.appendChild (b)
		b.textContent = 'きいています。'
		let placeholer = document.getElementById ('placeholder');
		placeholder.appendChild (p)
		document.getElementById ('nav-bottom').scrollIntoView ({'block': 'end'})
	}

	try {
		if (! document.getElementById ('checkbox-smartphone').checked) {
			document.getElementById ('audio-start').play ()
		}
	} catch (error) {
		console.log (error)
	}
}


recognition.addEventListener ('result', function (event) {
	const lastResult = event.results[event.results.length - 1]

	console.log('Confidence: ' + lastResult[0].confidence);

	const transcript = lastResult[0].transcript

	const request = new XMLHttpRequest ()
	request.open ('POST', '/post')
	request.send (transcript)

	const p = document.createElement ('p')
	p.textContent = transcript
	let placeholer = document.getElementById ('placeholder');
	placeholder.appendChild (p)
	document.getElementById ('nav-bottom').scrollIntoView ({'block': 'end'})

	try {
		if (! document.getElementById ('checkbox-smartphone').checked) {
			document.getElementById ('audio-ok').play ()
		}
	} catch (error) {
		console.log (error)
	}

	if (transcript.endsWith ('が見たい')) {
		let p = document.createElement ('p')
		let b = document.createElement ('b')
		p.appendChild (b)
		b.textContent = 'どうしてそんなものが見たいのですか？'
		let placeholer = document.getElementById ('placeholder');
		placeholder.appendChild (p)
		document.getElementById ('nav-bottom').scrollIntoView ({'block': 'end'})
		window.setTimeout (function () {
			const url =
				'https://duckduckgo.com/?q=' +
				encodeURIComponent (transcript.split ('が見たい')[0]) +
				'&iax=images&ia=images'
			window.open (url, '_blank')
		}, 2 * 1000)
	}
}, false)


recognition.addEventListener ('error', function(event) {
	{
		let p = document.createElement ('p')
		let b = document.createElement ('b')
		p.appendChild (b)
		b.textContent = event.error
		let placeholer = document.getElementById ('placeholder');
		placeholder.appendChild (p)
		document.getElementById ('nav-bottom').scrollIntoView ({'block': 'end'})
		let attr = new SpeechSynthesisUtterance (event.error)
		attr.lang= 'en-US'
		synth.speak (attr)
	}

	{
		let p = document.createElement ('p')
		let b = document.createElement ('b')
		p.appendChild (b)
		b.textContent = "再起動します。"
		let placeholer = document.getElementById ('placeholder');
		placeholder.appendChild (p)
		document.getElementById ('nav-bottom').scrollIntoView ({'block': 'end'})
		try {
			if (! document.getElementById ('checkbox-smartphone').checked) {
				document.getElementById ('audio-reboot').play ()
			}
		} catch (error) {
			console.log (error)
		}
	}

	recognition.stop()

	window.setTimeout (function () {
		startRecognition ()
	}, 10 * 1000)
}, false)

