window.addEventListener ('load', function () {
getPostsAndShow ()
window.setInterval (function () {
	getPostsAndShow ()
}, 10 * 1000) /* window.setInterval (function () { */
}, false); /* window.addEventListener ('load', function () { */


function getPostsAndShow () {
	const request = new XMLHttpRequest ()
	request.open ('GET', '/messages')
	request.addEventListener ('load', function () {
		if (request.status == 200) {
			const responseText = request.responseText;
			showPosts (responseText)
		}
	}, false)
	request.send ()
}


function showPosts (responseText) {
	const messages = responseText.split ("\n")

	const paceholder = document.getElementById ('placeholder');
	while (paceholder.firstChild) {
		paceholder.removeChild (placeholder.firstChild)
	}

	for (let cn = 0; cn < messages.length; cn ++) {
		try {
			const message = messages[cn]
			const p = document.createElement ('p')
			if (message.endsWith ('が見たい')) {
				const a = document.createElement ('a')
				a.innerText = message.split ('が見たい')[0]
				const url =
					'https://duckduckgo.com/?q=' +
					encodeURIComponent (message.split ('が見たい')[0]) +
					'&iax=images&ia=images'
				a.setAttribute ('href', url)
				a.setAttribute ('target', '_blank')
				p.appendChild (a)
				const text = document.createTextNode ('が見たい')
				p.appendChild (text)
			} else {
				const text = document.createTextNode (message)
				p.appendChild (text)
			}
			placeholder.appendChild (p)
		} catch (error) {
			console.log (error)
		}
	}

	const autoscroll = document.getElementById ('checkbox-autoscroll').checked
	if (autoscroll) {
		document.getElementById ('nav-bottom').scrollIntoView ({'block': 'end'})
	}
}


